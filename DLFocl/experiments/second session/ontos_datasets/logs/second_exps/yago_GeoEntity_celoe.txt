INIT KB <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
file:////C:/Users/Giuseppe/Documents/yago.owl
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by com.google.inject.internal.cglib.core.$ReflectUtils$2 (file:/C:/Users/Giuseppe/Downloads/dllearner-1.3.0/dllearner-1.3.0/lib/guice-4.0.jar) to method java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int,java.security.ProtectionDomain)
WARNING: Please consider reporting this to the maintainers of com.google.inject.internal.cglib.core.$ReflectUtils$2
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:////C:/Users/Giuseppe/Documents/yago.owl
Consistency check: true

CLASSES
---------------------------- 30

OBJECT Props

---------------------------- 0

DATA Props

---------------------------- 0

INDIVIDUALS

---------------------------- 206

CLASS DISJOINTNESS AXIOMS

---------------------------- 4


KB LOADED. 


Targets: [yagoGeoEntity]
NrandConcepts: 1

--------------- creating GOLD STANDARD CLASSIFICATION ---------------

TARGET Concepts: 1
PosExamples111
Counter exs:80
Uncertain: -65
Examples from :     Thing 206
Negated Concept: wikicat_16th-century_Christian_church_councils or wikicat_750_mm_gauge_railways or wikicat_785_mm_gauge_railways_in_Germany or wikicat_985_mm_gauge_railways or wikicat_Academies_in_Enfield or wikicat_Academies_in_Kent or wordnet_organization_108008335 or _domestic_association_football_leagues or _domestic_association_football_leagues or _ice_hockey_leagues or Nothing
concept: yagoGeoEntity	   pos:111     neg:80     und:15
generator null?false
10-fold CROSS VALIDATION Experiment on ontology: file:////C:/Users/Giuseppe/Documents/yago.owl
No of folds: 10
No of examples: 206
No of places: 210
No of examples per fold: 21


Fold #0 **************************************************************************************************
#training examples 185 - fold: 0

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
101  +  70
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (59.06%) class expression found after 98ms: Thing
more accurate (100.00%) class expression found after 2s575ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 0ms, 195 descriptions tested, 57 nodes in the search tree).

number of retrievals: 198
retrieval reasoning time: 9s 90ms ( 45ms per retrieval)
overall reasoning time: 9s 90ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #0

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.381      0.143      0.476      0.000      0.727      0.727
----------------------------------------------------------------------------------------------
  AVERAGES      0.381      0.143      0.476      0.000      0.727      0.727


Fold #1 **************************************************************************************************
#training examples 185 - fold: 1

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
99  +  71
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.24%) class expression found after 4ms: Thing
more accurate (100.00%) class expression found after 2s436ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 8ms, 208 descriptions tested, 61 nodes in the search tree).

number of retrievals: 220
retrieval reasoning time: 8s 130ms ( 36ms per retrieval)
overall reasoning time: 9s 740ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #1

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.238      0.238      0.524      0.000      0.545      0.462
----------------------------------------------------------------------------------------------
  AVERAGES      0.238      0.238      0.524      0.000      0.545      0.462


Fold #2 **************************************************************************************************
#training examples 185 - fold: 2

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
99  +  73
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (57.56%) class expression found after 3ms: Thing
more accurate (100.00%) class expression found after 1s549ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 18ms, 216 descriptions tested, 61 nodes in the search tree).

number of retrievals: 233
retrieval reasoning time: 9s 53ms ( 41ms per retrieval)
overall reasoning time: 9s 53ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #2

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.190      0.190      0.524      0.095      0.455      0.385
----------------------------------------------------------------------------------------------
  AVERAGES      0.190      0.190      0.524      0.095      0.455      0.385


Fold #3 **************************************************************************************************
#training examples 185 - fold: 3

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
98  +  73
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (57.31%) class expression found after 2ms: Thing
more accurate (100.00%) class expression found after 1s651ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 0ms, 210 descriptions tested, 61 nodes in the search tree).

number of retrievals: 207
retrieval reasoning time: 9s 201ms ( 42ms per retrieval)
overall reasoning time: 9s 201ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #3

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.333      0.190      0.476      0.000      0.636      0.500
----------------------------------------------------------------------------------------------
  AVERAGES      0.333      0.190      0.476      0.000      0.636      0.500


Fold #4 **************************************************************************************************
#training examples 185 - fold: 4

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
100  +  71
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.48%) class expression found after 4ms: Thing
more accurate (100.00%) class expression found after 1s571ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 21s,  201 descriptions tested, 59 nodes in the search tree).

number of retrievals: 211
retrieval reasoning time: 9s 222ms ( 46ms per retrieval)
overall reasoning time: 9s 222ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #4

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.190      0.238      0.524      0.048      0.455      0.417
----------------------------------------------------------------------------------------------
  AVERAGES      0.190      0.238      0.524      0.048      0.455      0.417


Fold #5 **************************************************************************************************
#training examples 185 - fold: 5

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
100  +  72
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.14%) class expression found after 1ms: Thing
more accurate (100.00%) class expression found after 1s655ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 3ms, 161 descriptions tested, 40 nodes in the search tree).

number of retrievals: 164
retrieval reasoning time: 9s 210ms ( 60ms per retrieval)
overall reasoning time: 9s 210ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #5

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.333      0.143      0.476      0.048      0.636      0.583
----------------------------------------------------------------------------------------------
  AVERAGES      0.333      0.143      0.476      0.048      0.636      0.583


Fold #6 **************************************************************************************************
#training examples 185 - fold: 6

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
101  +  73
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.05%) class expression found after 3ms: Thing
more accurate (100.00%) class expression found after 2s26ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 30ms, 203 descriptions tested, 59 nodes in the search tree).

number of retrievals: 215
retrieval reasoning time: 9s 233ms ( 46ms per retrieval)
overall reasoning time: 9s 233ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #6

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.286      0.190      0.429      0.095      0.455      0.455
----------------------------------------------------------------------------------------------
  AVERAGES      0.286      0.190      0.429      0.095      0.455      0.455


Fold #7 **************************************************************************************************
#training examples 185 - fold: 7

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
97  +  74
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (56.73%) class expression found after 2ms: Thing
more accurate (100.00%) class expression found after 1s574ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 12ms, 201 descriptions tested, 57 nodes in the search tree).

number of retrievals: 213
retrieval reasoning time: 9s 220ms ( 46ms per retrieval)
overall reasoning time: 9s 220ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #7

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.381      0.143      0.476      0.000      0.727      0.533
----------------------------------------------------------------------------------------------
  AVERAGES      0.381      0.143      0.476      0.000      0.727      0.533


Fold #8 **************************************************************************************************
#training examples 185 - fold: 8

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
101  +  71
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.72%) class expression found after 2ms: Thing
more accurate (100.00%) class expression found after 1s708ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 0ms, 192 descriptions tested, 57 nodes in the search tree).

number of retrievals: 193
retrieval reasoning time: 9s 210ms ( 50ms per retrieval)
overall reasoning time: 9s 210ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #8

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.238      0.143      0.524      0.095      0.545      0.545
----------------------------------------------------------------------------------------------
  AVERAGES      0.238      0.143      0.524      0.095      0.545      0.545


Fold #9 **************************************************************************************************
#training examples 189 - fold: 9

--------- Target Concept #0 
yagoGeoEntity

Learning problem prepared.
103  +  72
http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
entityExpansionLimit not supported by parser type org.apache.xerces.jaxp.SAXParserImpl
Notice: root element does not have an xml:base. Relative IRIs will be resolved against file:/C:/Users/Giuseppe/Documents/yago.owl
OntologyID(OntologyIRI(<file:////C:/Users/Giuseppe/Documents/yago.owl>) VersionIRI(<null>))
Loaded reasoner: null (org.semanticweb.HermiT.Reasoner)
start class:Thing
more accurate (58.86%) class expression found after 2ms: Thing
more accurate (100.00%) class expression found after 1s550ms: not (_ice_hockey_leagues)
Algorithm terminated successfully (time: 10s 8ms, 182 descriptions tested, 48 nodes in the search tree).

number of retrievals: 185
retrieval reasoning time: 9s 220ms ( 53ms per retrieval)
overall reasoning time: 9s 220ms

solutions:
1: not (_ice_hockey_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
2: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
3: not (_domestic_association_football_leagues) (pred. acc.: 100.00%, F-measure: 100.00%)
4: not (wordnet_organization_108008335) (pred. acc.: 100.00%, F-measure: 100.00%)
5: not (wikicat_Academies_in_Kent) (pred. acc.: 100.00%, F-measure: 100.00%)
6: not (wikicat_Academies_in_Enfield) (pred. acc.: 100.00%, F-measure: 100.00%)
7: not (wikicat_985_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
8: not (wikicat_785_mm_gauge_railways_in_Germany) (pred. acc.: 100.00%, F-measure: 100.00%)
9: not (wikicat_750_mm_gauge_railways) (pred. acc.: 100.00%, F-measure: 100.00%)
10: not (wikicat_16th-century_Christian_church_councils) (pred. acc.: 100.00%, F-measure: 100.00%)

FINAL: �_ice_hockey_leagues
induced for target #0:

yagoGeoEntity 




 |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #9

  TargetC#   matching commission   omission  induction  precision     recall
        0.      0.353      0.176      0.471      0.000      0.667      0.667
----------------------------------------------------------------------------------------------
  AVERAGES      0.353      0.176      0.471      0.000      0.667      0.667



 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ OVERALL OUTCOMES

  TargetC#   matching commission   omission  induction  precision     recall
         0      0.292      0.180      0.490      0.038      0.585      0.527 
----------------------------------------------------------------------------------------------
  AVERAGES      0.292      0.180      0.490      0.038      0.585      0.527 	 F-measure:      0.555
