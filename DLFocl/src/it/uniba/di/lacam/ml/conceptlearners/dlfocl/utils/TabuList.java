package it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils;

import java.util.ArrayList;
import java.util.HashSet;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;

/**
 *  A Tabu list for tabu search 
 * @author Giuseppe Rizzo
 *
 */
public class TabuList extends AbstractTabuList{
	private ArrayList<OWLClassExpression> table;
	private LProblem prob;

	public TabuList (LProblem prob) {
		this.prob= prob;
		
		table = new ArrayList<OWLClassExpression>();
		
	}
	
	
	public boolean add(OWLClassExpression c) {
		if (table.contains(c)) return true;
			return table.add(c);
	} 
	
	
	public boolean isNotAdmissible(OWLClassExpression c) {
		
		if (table.isEmpty())
			return false;
		else
			return  (table.contains(c));//stream().anyMatch((f)-> prob.reasoner.isEntailed(prob.dataFactory.getOWLSubClassOfAxiom(c, f))));
	 
	}

	public boolean remove() {
		
		return table.remove( table.get(0));
	} 
	
	public long size() {
		
		return table.size();
	}
}
