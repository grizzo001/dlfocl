/**
 * 
 */
package it.uniba.di.lacam.ml.conceptlearners.dlfoil;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;

import it.uniba.di.lacam.ml.conceptlearners.InductiveClassificationModel;
import it.uniba.di.lacam.ml.conceptlearners.LProblem;

/**
 *Wrapper for DL-Foil
 * @author NF
 *
 */
public class CEClassificationModel extends InductiveClassificationModel {
	
	protected OWLClassExpression model;
	
	public CEClassificationModel(LProblem aProblem) {
		super(aProblem);
	}

	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		//int score =-1;
		
		//do {
		
		model = DLFoil.induceConcept(problem, posExs, negExs, undExs); 
		
	
	};

	public  int classify(OWLIndividual ind) {
		if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(model,ind))) 
			return +1;
		else if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(model.getComplementNNF(),ind))) 
			return -1;
		else
			return 0;
	}

} // class
