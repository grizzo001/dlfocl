package it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.semanticweb.owlapi.model.OWLClassExpression;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;

public class ProbabilisticTabuList  extends TabuList{
	
	private BloomFilter<String> table;
	private BloomFilter<String> deletions; // a second bloom filter for maintaining deletions
	private LProblem prob;

	public ProbabilisticTabuList (LProblem lp) {
		super(lp);
		this.prob= prob;
		
		table  = BloomFilter.create(Funnels.unencodedCharsFunnel(), lp.nCandSubConcepts,0.10); 
		 deletions = BloomFilter.create(Funnels.unencodedCharsFunnel(), lp.nCandSubConcepts,0.10);
		
	}
	
	
	public boolean add(OWLClassExpression c) {
		//System.out.println("classexp."+c);
		boolean b = c !=null && (!table.mightContain(c.toString()) || (deletions.mightContain(c.toString()))) ;
		if (b) { 
			return table.put(c.toString());
		//return true;
		}
		return true;
	} 
	
	
	public boolean isNotAdmissible(OWLClassExpression c) {
		
		//if (table.isEmpty())
			//return false;
		//else
		//System.out.println("Expected number of elements" +table.approximateElementCount());
		return  table.mightContain(c.toString()) && ! deletions.mightContain(c.toString()); //table.getIfPresent(c)!=null;
		//	return  (table.contains(c));//stream().anyMatch((f)-> prob.reasoner.isEntailed(prob.dataFactory.getOWLSubClassOfAxiom(c, f))));
	 
	}

	public boolean remove() {
		
		deletions.putAll(table);
		return true;
	} 
	
	public long size() {
		
		return table.approximateElementCount()-deletions.approximateElementCount()>0?table.approximateElementCount()-deletions.approximateElementCount():0;
	}
	

}
