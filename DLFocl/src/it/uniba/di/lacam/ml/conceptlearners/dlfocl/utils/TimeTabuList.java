package it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.semanticweb.owlapi.model.OWLClassExpression;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;

public class TimeTabuList  extends TabuList{
	
	private LoadingCache<OWLClassExpression,OWLClassExpression> table;
	private LProblem prob;

	public TimeTabuList (LProblem lp) {
		super(lp);
		this.prob= prob;
		
		CacheLoader<OWLClassExpression,OWLClassExpression> loader = new CacheLoader<OWLClassExpression,OWLClassExpression>(){
			
			
			@Override
			public OWLClassExpression load(OWLClassExpression arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0;
			}
		};
		
		table = CacheBuilder.newBuilder().maximumSize(1000).expireAfterWrite(20,TimeUnit.MILLISECONDS).build(loader);
		
	}
	
	
	public boolean add(OWLClassExpression c) {
		//System.out.println("classexp."+c);
		boolean b = c !=null && table.getIfPresent(c)==null;
		if (b) { 
			table.put(c, c);
		return true;
		}
		return true;
	} 
	
	
	public boolean isNotAdmissible(OWLClassExpression c) {
		
		//if (table.isEmpty())
			//return false;
		//else
		return table.getIfPresent(c)!=null;
		//	return  (table.contains(c));//stream().anyMatch((f)-> prob.reasoner.isEntailed(prob.dataFactory.getOWLSubClassOfAxiom(c, f))));
	 
	}

	public boolean remove() {
		
		table.cleanUp();
		return true;
	} 
	
	public long size() {
		
		return table.size();
	}
	

}
