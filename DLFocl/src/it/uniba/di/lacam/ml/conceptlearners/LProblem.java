package it.uniba.di.lacam.ml.conceptlearners;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
//import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
//import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
//import org.semanticweb.HermiT.Reasoner;
//import org.semanticweb.HermiT.Reasoner.*;

import uk.ac.manchester.cs.jfact.JFactFactory;

import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.expression.OWLEntityChecker;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxOWLObjectRendererImpl;
import org.semanticweb.owlapi.manchestersyntax.renderer.ParserException;
import org.semanticweb.owlapi.util.BidirectionalShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;


import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.sun.org.apache.xerces.internal.impl.dtd.models.DFAContentModel;
import com.sun.prism.impl.Disposer.Target;

import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl1.DLFocl1Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl2.DLFocl2Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl3.DLFocl3Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl4.DLFocl4Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl6.DLFocl6Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl7.DLFocl7Model;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel2;
import it.uniba.di.lacam.ml.conceptlearners.tdt.TDTClassificationModel;





/**
 * Representation of a (supervised) learning problem: 
 * 		default settings for the parameters, 
 * 		ontology/reasoner management objects,
 * 
 * 		 
 * @author NF
 *
 */
public class LProblem {

	//	Properties settings = new Properties();
	public XMLConfiguration settings;	// non default

	public int nRandTargetConcepts = -1; // > 0
	public int nFolds = -1;
	public int nCandSubConcepts = -1;
	public int lookahead=2;
	public int MIN_COV_POS = 0;
	public int MIN_COV_NEG=0;
	public int MIN_POS2COV=0;
	public int TRIALS=1;
	public int LENGTH=-1;

	public int seed = -1;	// default seed for the random generator

	public IRI iri;  // the ontology IRI
	public String urlOwlFile;
	public String negExsPolicy;

	public OWLOntologyManager manager;
	public OWLDataFactory dataFactory;
	public OWLOntology ontology;
	public OWLReasoner reasoner;
	public OWLReasonerFactory reasonerFactory; //= new JFactFactory();
	public String design;
	public int nOfclimbings;
	public OWLNamedIndividual[] allIndividuals;
	public OWLNamedIndividual[] allExamples;
	public OWLClass[] allConcepts;
	public OWLObjectProperty[] allRoles;
	public OWLDataProperty[] allDataProperties;
	public Table<OWLNamedIndividual,OWLDataProperty, OWLLiteral> dpvalues;

	public OWLClassExpression[] testConcepts, negTestConcepts;	// concepts to be learned + their complements
	//	OWLNamedIndividual[] trainingExamples; // training examples
	public OWLClass top; // top concept (Thing)

	//	ArrayList<Integer>[] positiveExs; // all positive exs
	//	ArrayList<Integer>[] negativeExs;
	//	ArrayList<Integer>[] unlabeledExs;

	int[][] classification; // deductive classification [c][i] w.r.t. c-th class of the i-th individual 

	Map<String, Class<?>> algoMap = new HashMap<String, Class<?>>();

	List<Class<?>> algoTypes;
	public String exsRepo;


	/**
	 * @param clnArg
	 */
	@SuppressWarnings("deprecation")
	public
	LProblem(String clnArg) {

		// create and load default properties

		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<XMLConfiguration> builder =
				new FileBasedConfigurationBuilder<XMLConfiguration>(XMLConfiguration.class).configure(params.xml().setFileName(clnArg));

		//		FileInputStream in;
		try {
			settings = builder.getConfiguration();
			//			in = new FileInputStream(clnArg);
			//			settings.loadFromXML(in);			
			//			in.close();
			//		} catch (FileNotFoundException e) {
			//			System.out.println("File Not Found:"+clnArg);
			//			e.printStackTrace();
			//		} catch (IOException e) {
			//			System.out.println("I/O problem with "+clnArg);
		} catch (ConfigurationException ce) {
			System.out.println("Config file problem with "+clnArg);
			System.exit(1);
		}

		//	    // trim off any excess whitespace from properties
		//	    for (Object tmpKey : Collections.list(settings.propertyNames())) {
		//	      String tmpValue = settings.getProperty((String) tmpKey);
		//	      tmpValue = tmpValue.trim();
		//	      settings.put(tmpKey, tmpValue);
		//	    }

		//		settings.list(System.out);



		System.out.println("INIT KB <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");


		//		urlOwlFile = settings.getProperty("KB");

		urlOwlFile = settings.getString("kb.source"); // using relative paths
		reasonerFactory= settings.getString("kb.reasoner").compareToIgnoreCase("hermit")==0?new ReasonerFactory(): new JFactFactory() ;


		Locale.setDefault(Locale.US);

		iri = IRI.create(urlOwlFile);


		initKB();


		// read from config

		//		seed = Integer.parseInt(settings.getProperty("SEED"));
		seed = settings.getInt("seed");
		RandomGenerator.init(seed);

		//		nRandTargetConcepts = Integer.parseInt(settings.getProperty("NT"));
		nRandTargetConcepts = settings.getInt("evaluation.NRT");

		//		nFolds = Integer.parseInt(settings.getProperty("NF"));
		nFolds = settings.getInt("evaluation.NF");

		//		nCandSubConcepts = Integer.parseInt(settings.getProperty("NC"));
		nCandSubConcepts = settings.getInt("algo.NC");
		nOfclimbings = settings.getInt("algo.nClimbs");
		TRIALS= settings.getInt("algo.trials");
		MIN_POS2COV =settings.getInt("algo.minPos2Cov");
		MIN_COV_POS =settings.getInt("algo.minCovPos");
		MIN_COV_NEG =settings.getInt("algo.minCovNeg");
		LENGTH= settings.getInt("algo.length");

		// 		TODO abs algo
		List<Object>algoTypeStr = settings.getList("algo.types.type");
		System.out.println(algoTypeStr);
		algoTypes = new ArrayList<Class<?>>(); 
		if (algoTypeStr != null) {
			algoMap.put("DLFoil", CEClassificationModel.class); //inti
			algoMap.put("DLFoil2", CEClassificationModel2.class);
			algoMap.put("DLFocl1", DLFocl1Model.class);
			algoMap.put("DLFocl2", DLFocl2Model.class);
			algoMap.put("DLFocl3", DLFocl3Model.class);
			algoMap.put("DLFocl4", DLFocl4Model.class);
			algoMap.put("DLFocl6", DLFocl6Model.class);
			algoMap.put("DLFocl7", DLFocl7Model.class);
			algoMap.put("TDT", TDTClassificationModel.class);
			for (Object algo:algoTypeStr) {
				Class<?> class1 = algoMap.get(algo);
				System.out.println(algo);
				algoTypes.add(class1);
			}
			//algoType = algoMap.get(algoTypeStr);
		}
		else 
			algoTypes.add(CEClassificationModel.class);

		//		String targetClass = settings.getProperty("TARGET");

		design = settings.getString("evaluation.design");

		//String topConcept4LP= settings.getString("evaluation.top"); 
		List<Object> targets = settings.getList("evaluation.targets.target");
		System.out.println("Targets: "+targets);
		System.out.println("NrandConcepts: "+nRandTargetConcepts);


		if (!targets.isEmpty()) {

			nRandTargetConcepts = targets.size();

			testConcepts = new OWLClassExpression[nRandTargetConcepts];
			// read and convert list of target class expressions	

			for (int c=0; c < nRandTargetConcepts; c++) {

				ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
				Set<OWLOntology> importsClosure = ontology.getImportsClosure();

				BidirectionalShortFormProvider bidiShortFormProvider = 
						new BidirectionalShortFormProviderAdapter(manager, importsClosure, shortFormProvider);

				ManchesterOWLSyntaxParser parser = OWLManager.createManchesterParser();
				parser.setStringToParse((String) targets.get(c));
				parser.setDefaultOntology(ontology);

				OWLEntityChecker entityChecker = new ShortFormEntityChecker(bidiShortFormProvider);
				parser.setOWLEntityChecker(entityChecker);

				try {
					testConcepts[c] = parser.parseClassExpression();
				}
				catch (ParserException pe) {
					System.err.printf("Parsing problem with %s\n\n%s", targets.get(c), pe.getMessage());
					System.exit(1);
				}
			}
		}
		else {
			if (nRandTargetConcepts>0)
			 testConcepts = RandomGenerator.generateTargetConcepts(this, nRandTargetConcepts);
			
		}

		// generate class expr for the complement classes of the test concepts
		negExsPolicy= settings.getString("evaluation.negExs");
		exsRepo = settings.getString("evaluation.exDir");
		negTestConcepts = nRandTargetConcepts>0?new OWLClassExpression[testConcepts.length]:null;
		if (negExsPolicy.compareToIgnoreCase("allinds")==0) { 
			for (int c=0; c<testConcepts.length; c++) 
				negTestConcepts[c] = dataFactory.getOWLObjectComplementOf(testConcepts[c]);
			//		generate pos, neg instances for all test concept 
			generateExamples();
		}
		else if (negExsPolicy.compareToIgnoreCase("siblings")==0) {
			generateExamplesSiblingPolicy();
			
			
		}
		else {
			generateExamplesFromFiles(exsRepo);
		}

	}


	/**
	 * 
	 */
	void initKB() {

		System.out.println(iri);		

		try {
			manager = OWLManager.createOWLOntologyManager();
			dataFactory = manager.getOWLDataFactory();
			ontology = manager.loadOntology(iri);

			//			reasoner = ReasonerFactory.getInstance().createReasoner(ontology);
			//	        reasonerFactory = new PelletReasonerFactory();
			//			reasoner = reasonerFactory.createReasoner(ontology); 	


			//	        OWLReasonerConfiguration config = new SimpleConfiguration();
			reasoner = reasonerFactory.createReasoner(ontology);

			//			System.out.println("Precomputing inferences:");
			//	        reasoner.precomputeInferences(InferenceType.CLASS_ASSERTIONS);     


			boolean consistent = reasoner.isConsistent();
			System.out.printf("Consistency check: %s\n", consistent);
			if (!consistent) {
				System.err.println("FATAL ERROR\n");
				System.exit(1);
			}

		}
		catch(UnsupportedOperationException exception) {
			System.out.println("Unsupported reasoner operation.");
			System.exit(1);
		}
		catch (OWLOntologyCreationException e) {
			System.out.println("Could not load the ontology: " + e.getMessage());
			System.exit(1);
		}

		System.out.println("\nCLASSES");
		Stream<OWLClass> classList = ontology.classesInSignature();
		allConcepts = classList.toArray(size -> new OWLClass[size]); // new Java8 version
		//		allConcepts = new OWLClass[classList.size()];

		//		int c=0;
		//        for(OWLClass cls : classList) {
		//			if (!cls.isOWLNothing() && !cls.isAnonymous()) {
		//				allConcepts[c++] = cls;
		////				System.out.println(cls);
		//			}	        		
		//		}
		System.out.println("---------------------------- "+allConcepts.length);

		System.out.println("\nOBJECT Props\n");
		//        Set<OWLObjectProperty> propList = ontology.getObjectPropertiesInSignature(); // deprecated in OWLAPI 5
		Stream<OWLObjectProperty> propList = ontology.objectPropertiesInSignature();
		allRoles = propList.toArray(size -> new OWLObjectProperty[size]); // new Java8 version

		//        allRoles = propList.toArray(new OWLObjectProperty[propList.size()]);

		//		allRoles = new OWLObjectProperty[propList.size()];
		//		int op=0;
		//        for(OWLObjectProperty prop : propList) {
		//			if (!prop.isAnonymous()) {
		//				allRoles[op++] = prop;
		////				System.out.println(prop);
		//			}	        		
		//		}

		System.out.println("---------------------------- "+allRoles.length);

		System.out.println("\nINDIVIDUALS\n");
		//      Set<OWLNamedIndividual> indList = ontology.getIndividualsInSignature();
		Stream<OWLNamedIndividual> indList = ontology.individualsInSignature();
		//      allIndividuals = indList.toArray(new OWLNamedIndividual[indList.size()]);
		allIndividuals = indList.toArray(size -> new OWLNamedIndividual[size]);
		//        allIndividuals = new OWLNamedIndividual[indList.size()];
		//      int i=0;
		//      for(OWLNamedIndividual ind : indList) {
		//			if (!ind.isAnonymous()) {
		//				allIndividuals[i++] = ind;
		////				System.out.println(ind);
		//			}	        		
		//		}
		System.out.println("---------------------------- "+allIndividuals.length);   

		//load dataproperties only when you are using Hermit
		if (!(reasonerFactory instanceof JFactFactory)) {
			System.out.println("\nDATA Props\n");
			//        Set<OWLDataProperty> dataPropSet = ontology.getDataPropertiesInSignature();
			Stream<OWLDataProperty> dataPropSet = ontology.dataPropertiesInSignature();
			//		allDataProperties = dataPropSet.toArray(new OWLDataProperty[dataPropSet.size()]);
			allDataProperties = dataPropSet.toArray(size -> new OWLDataProperty[size]);
			//		System.out.println(dataPropSet);
			dpvalues= HashBasedTable.create();
			//PelletReasoner pelletReasoner=  new PelletReasoner(ontology, BufferingMode.NON_BUFFERING);

			//dpvalues= HashBasedTable.create();

			for (int  i= 0;  i< allDataProperties.length; ++i) {
				OWLDataProperty owlDataProperty = allDataProperties[i];
				Set<OWLClass> domains = reasoner.getDataPropertyDomains(owlDataProperty).getFlattened();
				//System.out.println("dataProperties: "+ owlDataProperty +"   domains: "+ domains);
				for (OWLClass c: domains) {
					Set<OWLNamedIndividual> instances = reasoner.getInstances(c).getFlattened();
					for (OWLNamedIndividual ind : instances) {
						Set<OWLLiteral> values=reasoner.getDataPropertyValues(ind, owlDataProperty);
						if (!values.isEmpty()) {
							//System.out.println(ind + "dataProperties: "+ owlDataProperty +"values: "+values);
							for (OWLLiteral owlLiteral : values) {
								dpvalues.put(ind, owlDataProperty, owlLiteral);
							}
						}
					}
				}
			}


			//Set<OWLDataProperty> columnKeySet = dpvalues.columnKeySet();
			//Map<OWLDataProperty, Map<OWLNamedIndividual, OWLLiteral>> columnMap = dpvalues.columnMap();

			//System.out.println(dpvalues);
			System.out.println("---------------------------- "+allDataProperties.length);
		}



		System.out.println("\nCLASS DISJOINTNESS AXIOMS\n");
		long nDAxioms = ontology.getAxiomCount(AxiomType.DISJOINT_CLASSES,false);
		System.out.println("---------------------------- "+nDAxioms);   


		top = dataFactory.getOWLThing();


		System.out.println("\n\nKB LOADED. \n\n");	
	}

	//	examples generation for all test concepts
	/**
	 * 
	 */
	void generateExamples() {

		allExamples= new OWLNamedIndividual[allIndividuals.length];
		System.out.println(allExamples.length);
		allExamples=allIndividuals;

		System.out.printf("\n--------------- creating GOLD STANDARD CLASSIFICATION ---------------\n"); 
		System.out.printf("\nTARGET Concepts: %d\n", testConcepts.length); 

		classification = new int[testConcepts.length][allExamples.length];
		ArrayList<Integer>[] positiveExs = new ArrayList[testConcepts.length];
		ArrayList<Integer>[] negativeExs = new ArrayList[testConcepts.length];
		ArrayList<Integer>[] unlabeledExs = new ArrayList[testConcepts.length];
		OWLObjectRenderer renderer = new ManchesterOWLSyntaxOWLObjectRendererImpl();

		for(int c=0; c<testConcepts.length;c++) {
			System.out.printf("concept: %s\t", renderer.render(testConcepts[c])); 

			positiveExs[c] = new ArrayList<Integer>();
			negativeExs[c] = new ArrayList<Integer>();
			unlabeledExs[c] = new ArrayList<Integer>();
			//labeling(positiveExs, negativeExs, unlabeledExs, allExamples, testConcepts[c], negTestConcepts[c],c);
			
			for(int i=0; i<allExamples.length; i++){
				if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(testConcepts[c], allExamples[i]))) {
					positiveExs[c].add(i);
					classification[c][i] = +1;
				}
				else if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(negTestConcepts[c], allExamples[i]))) {

					negativeExs[c].add(i);
					classification[c][i] = -1;
				}
				else {

					// unlabed coming from superclasses t

					unlabeledExs[c].add(i);   // NEW 
					classification[c][i] = 0; 
					
				}

			}			
			System.out.printf("%10s %10s %10s\n", 
					"pos:"+positiveExs[c].size(), "neg:"+negativeExs[c].size(), "und:"+unlabeledExs[c].size());	
		} 


		//}	

	} // class


	void generateExamplesFromFiles(String dir) {
		Charset charset = Charset.forName("ISO-8859-1");
		allExamples= null;
		
		try {
			System.out.println(new File(dir+"pos.txt").getAbsolutePath());
			List<String> poslines = Files.readAllLines(Paths.get(new File(dir+"/pos.txt").getAbsolutePath()),charset);
			List<String> neglines= Files.readAllLines(Paths.get(new File(dir+"/neg.txt").getAbsolutePath()),charset);
			allExamples = new OWLNamedIndividual[poslines.size()+neglines.size()];
			classification = new int[1][allExamples.length];
			int i=0;
			for(String ind: poslines) {
			allExamples[i]= dataFactory.getOWLNamedIndividual(ind);
			classification[0][i]=+1;
			i++;
			}
			for(String ind: neglines) {
				allExamples[i]= dataFactory.getOWLNamedIndividual(ind);
				classification[0][i]=-1;
				i++;
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
	
	void generateExamplesSiblingPolicy() {



		System.out.printf("\n--------------- creating GOLD STANDARD CLASSIFICATION ---------------\n"); 
		System.out.printf("\nTARGET Concepts: %d\n", testConcepts.length); 




		ArrayList<Integer>[] positiveExs = new ArrayList[1];
		ArrayList<Integer>[] negativeExs = new ArrayList[1];
		ArrayList<Integer>[] unlabeledExs = new ArrayList[1];
		ArrayList<OWLNamedIndividual> allExs = new ArrayList<>();

		// extract the examples from the superclasses

		Set<OWLClass> superClasses = reasoner.getSuperClasses(testConcepts[0], true).getFlattened();
		superClasses.remove(dataFactory.getOWLNothing());; 


		//if (superClasses.contains(dataFactory.getOWLThing()) &&superClasses.size()>1)
		//superClasses.remove(dataFactory.getOWLThing());
		OWLClassExpression concept= dataFactory.getOWLObjectUnionOf(superClasses); 
		allExs.addAll(reasoner.getInstances(concept).getFlattened());
		Set<OWLClass> flattened = reasoner.getDisjointClasses(testConcepts[0]).getFlattened();
		OWLClassExpression disjClass = dataFactory.getOWLObjectUnionOf(flattened);
		negTestConcepts[0]=disjClass;
		int counter = reasoner.getInstances(disjClass).getFlattened().size();
		@SuppressWarnings("deprecation")
		int exs = reasoner.getInstances(concept).getFlattened().size() - counter;
		Set<OWLNamedIndividual> testConceptInstances = reasoner.getInstances(testConcepts[0]).getFlattened();
		System.out.println( "PosExamples"+testConceptInstances.size() );
		System.out.println( "Counter exs:"+counter);
		System.out.println( "Uncertain: "+ (exs-counter-testConceptInstances.size()));
		//System.out.println("Disj Class: "+disjClass);

		allExamples=allExs.toArray( new OWLNamedIndividual[allExs.size()]);



		OWLObjectRenderer renderer = new ManchesterOWLSyntaxOWLObjectRendererImpl();
		System.out.println("Examples from :     " +renderer.render(concept) +   " "+allExs.size());
		System.out.println("Negated Concept: " +renderer.render(disjClass));


		for(int c=0; c<testConcepts.length;c++) {
			System.out.printf("concept: %s\t", renderer.render(testConcepts[0])); 

			positiveExs[c] = new ArrayList<Integer>();
			negativeExs[c] = new ArrayList<Integer>();
			unlabeledExs[c] = new ArrayList<Integer>();


			labeling(positiveExs, negativeExs, unlabeledExs, allExamples, testConcepts[0],disjClass, c); 
			System.out.printf("%10s %10s %10s\n", 
					"pos:"+positiveExs[c].size(), "neg:"+negativeExs[c].size(), "und:"+unlabeledExs[c].size());	
		} 


	}


	private void labeling(ArrayList<Integer>[] positiveExs, ArrayList<Integer>[] negativeExs,
			ArrayList<Integer>[] unlabeledExs, OWLNamedIndividual[] allExs, OWLClassExpression target,OWLClassExpression negated, int c) {
		classification = new int[testConcepts.length][allExs.length];
		for(int i=0; i<allExs.length; i++){
			if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(target, allExs[i]))) {
				positiveExs[c].add(i);
				classification[c][i] = +1;
			}
			else if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(negated, allExs[i]))) {

				negativeExs[c].add(i);
				classification[c][i] = -1;
			}
			else {

				// unlabed coming from superclasses t

				unlabeledExs[c].add(i);   // NEW 
				classification[c][i] = 0; 
				
			}
		
		}
	}	

} // class


