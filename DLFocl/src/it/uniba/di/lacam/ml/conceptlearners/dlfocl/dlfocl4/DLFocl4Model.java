package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl4;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.DLFoil;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.DLFoil2;

/**
 * Implements a repeated hill-climbing approach as dlfocl1 with a match-oriented heuristic 
 * for starting the next iteration a DL-FOIL as  hill-climber
 * 
 * @author Giuseppe
 *
 */
public class DLFocl4Model extends CEClassificationModel {

	public DLFocl4Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * Modified training procedure exploiting {@link DLFoil2} instead of DL-Foil
	 */
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
				int nOfClimbs=problem.nOfclimbings;
				System.out.println("Starting repeated hill-climbing search");
		int bestScore,currentScore=Integer.MIN_VALUE;
		
		model = DLFoil2.induceConcept(problem, posExs, negExs, undExs); 
		OWLClassExpression bestModel= model;
		bestScore= score(posExs, negExs, undExs);
		
		
		do {
		
		model = DLFoil2.induceConcept(problem, posExs, negExs, undExs); 
		currentScore= score(posExs, negExs, undExs);
		if (currentScore<bestScore)
			model= bestModel; // return to the best model obtained so far  
			
		nOfClimbs--;
		}
		while( currentScore>bestScore && nOfClimbs>1); 
	};

	/**
	 * Auxiliary method for plugging more sophisticated score (currently supported: the match rate )
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @return
	 */
	private int score (ArrayList<Integer>posExs,ArrayList<Integer>negExs, ArrayList<Integer>undExs) {
		
		int score =-1;
		int matches =0;
		int commissions=0;
		int induction=0;
		int omissions =0;

		for (int i=0; i<posExs.size(); i++){
			int classify=classify(problem.allIndividuals[posExs.get(i)]);
			matches= classify ==1?(matches+1):matches;
			commissions= classify==-1?(commissions+1):commissions; 
			omissions= classify==0?(omissions+1):omissions; // omission
		}
				
		for (int i=0; i<negExs.size(); i++){
			int classify=classify(problem.allIndividuals[negExs.get(i)]);
			matches= classify ==-1?(matches+1):matches;
			commissions= classify==1?(commissions+1):commissions;
			omissions= classify==0?(omissions+1):omissions; // omission
		}
		for (int i=0; i<undExs.size(); i++) {
		   int classify=classify(problem.allIndividuals[undExs.get(i)]);
			matches= classify ==0?(matches+1):matches;
			induction= classify==1?(commissions+1):commissions;	
			
		}
		score = matches; //* commissions *omissions *induction; 
		return score;
		
	}

}
