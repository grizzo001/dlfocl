package it.uniba.di.lacam.ml.conceptlearners;

import java.util.HashSet;
import java.util.Random;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;




/**
 * @author NF
 *
 */
public class RandomGenerator {
	
	public static Random generator;
	
	/**
	 * @param seed
	 */
	static void init(int seed) {
		generator = new Random(seed);
	}

	
	
	/**
	 * @param prob
	 * @param nTargetConcepts
	 * @return
	 */
	static OWLClassExpression[] generateTargetConcepts(LProblem prob, int nTargetConcepts){
        
		System.out.println("\nTARGET CONCEPTS GENERATION");
		OWLClassExpression[] targetConcepts = new OWLClassExpression[nTargetConcepts];
        final int minOfSubConcepts = 2;
        final int maxOfSubConcepts = 5;
        int numOfSubConcepts = 0;
        int i, j;

        int nExs = prob.allIndividuals.length;
        
        OWLClassExpression nextConcept;
        OWLClassExpression complPartialConcept; 
        
        // loop building new target concepts
        for (i=0; i<nTargetConcepts; i++) {           
        	OWLClassExpression partialConcept; 
        	System.out.println(generator==null);
            numOfSubConcepts = minOfSubConcepts + generator.nextInt(maxOfSubConcepts-minOfSubConcepts+1);
            long numPosInst;
			long numNegInst;
            // build a single new target class adding conjuncts or disjuncts
            do {
//            	take the first subConcept for building the target class
                partialConcept = getRandomConcept(prob);
                
	            for (j=1; j < numOfSubConcepts; j++) {
	               
            		HashSet<OWLClassExpression> newConcepts = new HashSet<OWLClassExpression>();	            	
                	newConcepts.add(partialConcept);
                	nextConcept = getRandomConcept(prob);
                	newConcepts.add(nextConcept);	                
	                
	                if (generator.nextInt(3) ==0) 
	                	partialConcept = prob.dataFactory.getOWLObjectIntersectionOf(newConcepts);
	                else
	                    partialConcept = prob.dataFactory.getOWLObjectUnionOf(newConcepts);
	            } // for j
//            	System.out.println();
            	complPartialConcept = prob.dataFactory.getOWLObjectComplementOf(partialConcept);
                
                numPosInst = (prob.reasoner.getInstances(partialConcept,false).entities()).count(); 
                numNegInst = (prob.reasoner.getInstances(complPartialConcept,false).entities()).count();
                
                System.out.print("|");
//                System.out.printf("%s\n",partialConcept);
//                System.out.printf("pos:%d (%3.1f)\t\t neg:%d (%3.1f)\t\t und:%d (%3.1f)\n",
//                		numPosInst,numPosInst*100.0/nExs,
//                		numNegInst,numNegInst*100.0/nExs,
//                		(nExs-numNegInst-numPosInst), (nExs-numNegInst-numPosInst)*100.0/nExs);
            	
//            } while (!reasoner.isSatisfiable(partialConcept) || !reasoner.isSatisfiable(complPartialConcept));
            } while ((double)(numPosInst+numNegInst)/nExs < .1 
            			|| (double)(numPosInst)/nExs < .05 
            			|| (double)(numNegInst)/nExs < .05);
//						|| (double)(prob.allIndividuals.length-numPosInst)/nExs < .05);
            
            //add the newly built class to the list of all required targets
            targetConcepts[i] = partialConcept.getNNF();
            System.out.printf(" Target Concept %d found ", i);
//            System.out.printf("\n Target Concept %3d.  pos:%6d  neg:%6d \n", i, numPosInst, numNegInst);
        }
        System.out.println();
        return targetConcepts;
    }
	
	

	
	/**
	 * @param prob
	 * @return
	 */
	public static OWLClassExpression getRandomConcept(LProblem prob) {
		
		OWLClassExpression newConcept = null;
				
		do {
			if (generator.nextDouble() < 0.20) 
				newConcept = prob.allConcepts[generator.nextInt(prob.allConcepts.length)];
			else {
				OWLClassExpression newConceptBase;
				if (generator.nextDouble() < 0.2) 
					newConceptBase = getRandomConcept(prob);
				else
					newConceptBase = prob.allConcepts[generator.nextInt(prob.allConcepts.length)];
				if (generator.nextDouble() < 0.75) { // new role restriction
					OWLObjectProperty role = prob.allRoles[generator.nextInt(prob.allRoles.length)];
//					OWLDescription roleRange = (OWLDescription) role.getRange;
					
					if (generator.nextDouble() < 0.5)
						newConcept = prob.dataFactory.getOWLObjectAllValuesFrom(role, newConceptBase);
					else
						newConcept = prob.dataFactory.getOWLObjectSomeValuesFrom(role, newConceptBase);
				}
				else					
					newConcept = prob.dataFactory.getOWLObjectComplementOf(newConceptBase);
			} // else
//			System.out.printf("-->\t %s\n",newConcept);
		} while (newConcept==null || prob.reasoner.getInstances(newConcept,false).entities().count()==0);
//		} while (!prob.reasoner.isSatisfiable(newConcept));		
		
		return newConcept;		
	}	
	
	
	/**
	 * @param prob
	 * @param currentConcept
	 * @return
	 */
	public static OWLClassExpression getSubsumedRandomConcept(LProblem prob, OWLClassExpression currentConcept) {
		
		OWLClassExpression newConcept = null;
				
		do {
			if (generator.nextDouble() < 0.5) 
				newConcept = prob.allConcepts[generator.nextInt(prob.allConcepts.length)];
			else {
				OWLClassExpression newConceptBase;
				if (generator.nextDouble() < 0.5) 
					newConceptBase = getRandomConcept(prob);
				else
					newConceptBase = prob.allConcepts[generator.nextInt(prob.allConcepts.length)];
				if (generator.nextDouble() < 0.5) { // new role restriction
					OWLObjectProperty role = prob.allRoles[generator.nextInt(prob.allRoles.length)];
//					OWLDescription roleRange = (OWLDescription) role.getRange;
					
					if (generator.nextDouble() < 0.5)
						newConcept = prob.dataFactory.getOWLObjectAllValuesFrom(role, newConceptBase);
					else
						newConcept = prob.dataFactory.getOWLObjectSomeValuesFrom(role, newConceptBase);
				}
				else					
					newConcept = prob.dataFactory.getOWLObjectComplementOf(newConceptBase);
			} // else
//			System.out.printf("-->\t %s\n",newConcept);
//		} while (newConcept==null || !(reasoner.getIndividuals(newConcept,false).size() > 0));
		} while (!prob.reasoner.isSatisfiable(newConcept)); 
//				|| !prob.reasoner.isEntailed(prob.dataFactory.getOWLSubClassOfAxiom(currentConcept,newConcept)));		
		
		return newConcept.getNNF();		
	}	
	
} // class
