package it.uniba.di.lacam.ml.conceptlearners.coverfunctions;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;

public class CoverFunction {

	/**
	 * A wrapper methodfor the notion of coverage 
	 * @param prob
	 * @param concept
	 * @param covExs, the set of examples to be considered for assessing the coverage
	 * @return
	 */
	public static ArrayList<Integer> removeCovered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		
		ArrayList<Integer> unCovExs = new ArrayList<Integer>();
		
		for (Integer ex: covExs) {
			if (!prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(concept,prob.allExamples[ex]))) 
				unCovExs.add(ex);			 
		}
		return unCovExs;
	}

	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> covered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		
		ArrayList<Integer> newCovExs = new ArrayList<Integer>();
		
		for (Integer ex: covExs) {
			if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(concept, prob.allExamples[ex]))) 
				newCovExs.add(ex);
		}
	
		
		return newCovExs;
	}

}
