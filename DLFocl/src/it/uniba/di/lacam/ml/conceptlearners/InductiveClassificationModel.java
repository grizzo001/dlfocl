/**
 * 
 */
package it.uniba.di.lacam.ml.conceptlearners;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLIndividual;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;

/**
 * @author NF
 *
 */
public abstract class InductiveClassificationModel {
	
	protected LProblem problem;

	public InductiveClassificationModel(LProblem aProblem) {
		this.problem = aProblem;
	}
	
	public abstract void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs);

	public abstract int classify(OWLIndividual ind);

} // class
