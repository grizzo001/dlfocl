package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl7;

import java.util.ArrayList;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;

public class DLFocl7Model extends CEClassificationModel {
	

	public DLFocl7Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		
	
		System.out.println("Starting local search with tabu list");
		
		model = DLFocl7.induceConcept(problem, posExs, negExs, undExs); 
		
				 
	};


}
