package it.uniba.di.lacam.ml.conceptlearners.refops;

import java.util.HashSet;
import java.util.Random;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;

public class RefinementOperator {

	
	/**
	 * Refine a concept either adding a new conjunct or  replacing with a new concept subuming the currentone 
	 * 
	 * @param prob, the learning problem
	 * @param currConcept, the current concept
	 * @return
	 */
	static OWLClassExpression rhoSingle(Random  generator, LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement=null,	filler = null, refinedFiller=null;
		OWLObjectProperty property;
				
		if (generator.nextFloat() < 0.75)
			// refine concept
			switch (currConcept.getClassExpressionType()) {
			case OWL_CLASS: 
				OWLClass[] subCs = (OWLClass[]) prob.reasoner.getSubClasses(currConcept,false).entities().filter((f)->f.toString().compareTo(currConcept.toString())!=0).toArray(size -> new OWLClass[size]); ;
				refinement = (OWLClassExpression) subCs[generator.nextInt(subCs.length)];
				break;
			case OBJECT_COMPLEMENT_OF: 
				OWLClassExpression nestedConcept = ((OWLObjectComplementOf) currConcept).getOperand();
				OWLClass[] supCs = (OWLClass[]) prob.reasoner.getSuperClasses(nestedConcept,false).entities().filter((f)->f.compareTo(nestedConcept)!=0 && f.toString().compareTo(prob.testConcepts[0].asOWLClass().toString())!=0).toArray(size -> new OWLClass[size]);


				refinement = prob.dataFactory.getOWLObjectComplementOf(supCs[generator.nextInt(supCs.length)]); 
				break;
			case OBJECT_ALL_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectAllValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectAllValuesFrom(property, refinedFiller);
				break;
			case OBJECT_SOME_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectSomeValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectSomeValuesFrom(property, refinedFiller);
				break;
//			case DATA_ALL_VALUES_FROM:
//				break;
//			case DATA_EXACT_CARDINALITY:
//				break;
//			case DATA_HAS_VALUE:
//				break;
//			case DATA_MAX_CARDINALITY:
//				break;
//			case DATA_MIN_CARDINALITY:
//				break;
//			case DATA_SOME_VALUES_FROM:
//				break;
//			case OBJECT_EXACT_CARDINALITY:
//				break;
//			case OBJECT_HAS_SELF:
//				break;
//			case OBJECT_HAS_VALUE:
//				break;
//			case OBJECT_INTERSECTION_OF:
//				break;
//			case OBJECT_MAX_CARDINALITY:
//				break;
//			case OBJECT_MIN_CARDINALITY:
//				break;
//			case OBJECT_ONE_OF:
//				break;
//			case OBJECT_UNION_OF:
//				break;
			default:
				break;
			}
			else { // refine by adding conjunct
				OWLClassExpression newConcept = null;
				switch (RandomGenerator.generator.nextInt(4)) {
				case 0:
					newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					break;
				case 1:
					newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					newConcept = prob.dataFactory.getOWLObjectComplementOf(newConcept);
					break;
				case 2:
//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					filler=prob.top;
					property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
					newConcept = prob.dataFactory.getOWLObjectAllValuesFrom(property, filler);					
					break;
				case 3:
//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					filler=prob.top;
					property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
					newConcept = prob.dataFactory.getOWLObjectSomeValuesFrom(property, filler);					
					break;
				default:
					break;
				} // switch
				
				if (currConcept.equals(prob.top))
					refinement = newConcept;
				else {
					HashSet<OWLClassExpression> set = new HashSet<OWLClassExpression>();
					set.add(currConcept);
					set.add(newConcept); // was getRandomConcept
					refinement = prob.dataFactory.getOWLObjectIntersectionOf(set);
				}
			} // else add

		return refinement;

	}
	
	//  
	/**
	 * refine conjunct of CEs (length >1)	
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	static OWLClassExpression rhoCompound(Random generator, LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement=null;
		
								
		HashSet<OWLClassExpression> conjSet = (HashSet<OWLClassExpression>) currConcept.asConjunctSet();
		OWLClassExpression[] conjArray = conjSet.toArray(new OWLClassExpression[conjSet.size()]);	
		
		// select one of the CEs
		int selectedConjIndex = generator.nextInt(conjArray.length);
		OWLClassExpression conjunct2refine = conjArray[selectedConjIndex];
		// refine selected CE
		OWLClassExpression refinedConjunct = rho(generator,prob, conjunct2refine);
		// replace selected CE with the refinement 
		conjSet.remove(conjunct2refine);
		conjSet.add(refinedConjunct);

		refinement = prob.dataFactory.getOWLObjectIntersectionOf(conjSet);
			
		return refinement.getNNF();
	}
	
	
	
	/**
	 * Generate a refinement of the current concepr randomly applying either rhoSingle or rhoCompound
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	public static OWLClassExpression rho(Random generator, LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement;

		do {
		
			if (currConcept.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF)  
				// refine one of the conjuncts in the CE 
				refinement = rhoCompound(generator, prob, currConcept);
			else
				// replace with a refinement or add a new conjunct
				refinement = rhoSingle(generator,prob, currConcept);
		
//		} while (prob.reasoner.getInstances(refinement,false).entities().count()==0); // instead of deprecated method
		} while (!prob.reasoner.isSatisfiable(refinement));//|| refinement.containsConjunct(prob.testConcepts[0]));		
		
		return refinement;
	}

	
	
}
