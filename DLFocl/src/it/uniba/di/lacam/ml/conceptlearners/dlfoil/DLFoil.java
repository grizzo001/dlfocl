/**
 * 
 */
package it.uniba.di.lacam.ml.conceptlearners.dlfoil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.coverfunctions.CoverFunction;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator;
import it.uniba.di.lacam.ml.conceptlearners.scores.Heuristics;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;



/**
 * Improved version  of DL-Foil  (see Fanizzi et al@EKAW2018)
 * @author NF
 *
 */
public class DLFoil {

	
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer(); 
	

	
	/**
	 * @param prob
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @return
	 */
	public static OWLClassExpression induceConcept(LProblem prob, ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {		
		
		System.out.printf("\n Problem\t p:%d\t n:%d\t u:%d\t\n", 
				posExs.size(), negExs.size(), undExs.size());
		
		// set of disjuncts that make up the final concept
		HashSet<OWLClassExpression> disjuncts = new HashSet<OWLClassExpression>(); 
		
		ArrayList<Integer> pos2Cover = (ArrayList<Integer>)posExs.clone();
		OWLClassExpression newConcept = prob.top;
		OWLClassExpression finalConcept = prob.dataFactory.getOWLNothing(); // empty set --> bottom 

		ArrayList<Integer> covPos = null, covNeg = null, covUnd = null;		
		//while (pos2Cover.size()>0) { //&&  {		
		for (int i=0;i<prob.LENGTH;i++) {	
			
			covPos = (ArrayList<Integer>)pos2Cover.clone();
			covNeg = (ArrayList<Integer>)negExs.clone();
			covUnd = (ArrayList<Integer>)undExs.clone();
			System.out.println("Subproblem ------------------------------------------");
			while (covPos.size() > prob.MIN_COV_POS) {
				
				newConcept = getBestSpecialization(prob, newConcept, covPos, covNeg, covUnd);
				covPos = CoverFunction.covered(prob, newConcept, covPos);
				covNeg = CoverFunction.covered(prob, newConcept, covNeg);
				covUnd = CoverFunction.covered(prob, newConcept, covUnd);
				System.out.printf("\nSpecialization, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
				System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());
				
			}

			System.out.printf("\nDisjunct, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
			System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());

			disjuncts.add(newConcept);
			pos2Cover = CoverFunction.removeCovered(prob, newConcept, pos2Cover);
			newConcept = prob.top;
		
			
		}	
		
		if (disjuncts.size() == 1) 
			finalConcept = disjuncts.iterator().next(); // single disj.
		else 
			finalConcept = prob.dataFactory.getOWLObjectUnionOf(disjuncts); // multiple disj. 
		
		System.out.printf("\n FINAL: %s\n\n", renderer.render(finalConcept));
		return finalConcept;
		
	}
	
	
	/**
	 * @param prob
	 * @param currConcept
	 * @param covPos
	 * @param covNeg
	 * @param covUnd
	 * @return
	 */
	protected static OWLClassExpression getBestSpecialization(LProblem prob, OWLClassExpression currConcept, 
			ArrayList<Integer> covPos, ArrayList<Integer> covNeg, ArrayList<Integer> covUnd) {
	
		
	OWLClassExpression bestConcept = null;
	double bestGain = - Double.MAX_VALUE;	
	

	while (bestGain <0) {
		System.out.printf("\nSpecializing %s\n %d\t %d\t %d\n", 
				renderer.render(currConcept), covPos.size(), covNeg.size(), covUnd.size());
		
		
		for (int c=0; c<(prob.nCandSubConcepts); ++c) {
			ArrayList<Integer> 	newCovPos=null, newCovNeg=null, newCovUnd=null; 
			
			
			do {
				OWLClassExpression refConcept = RefinementOperator.rho(RandomGenerator.generator,prob, currConcept);
				
				// add a lookhead 
				//refConcept = rho(prob, refConcept);
				
				
				double thisGain = 0;
			
				newCovPos = CoverFunction.covered(prob, refConcept, covPos);
				if (newCovPos.size()>0) {
					newCovNeg = CoverFunction.covered(prob, refConcept, covNeg);
					newCovUnd = CoverFunction.covered(prob, refConcept, covUnd);
		
					thisGain = Heuristics.wig(covPos.size(), covNeg.size(), covUnd.size(), 
								   newCovPos.size(), newCovNeg.size(), newCovUnd.size());
					
					System.out.printf("%4d. %s\n\t%4d\t %4d\t %4d\t\t %+7e", 
							c, renderer.render(refConcept), newCovPos.size(), newCovNeg.size(), newCovUnd.size(), thisGain);
						
					if (thisGain > bestGain && thisGain > 0) {
						bestConcept = refConcept;
						bestGain = thisGain;
						System.out.println(" <--- current BEST");
					}
					else
						System.out.println();
				}
			} while (newCovPos.size()==0);
		} //
		if (bestGain < 0) {
			System.out.print("\nNo good specialization found. Trying again... \n");
		}
	} // while
	


	System.out.printf("************ best: %e\t %s\n", bestGain, renderer.render(bestConcept));
	
	return bestConcept;
	}
	
	
	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> removeCovered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		return CoverFunction.removeCovered(prob, concept, covExs);
	}
	
	
	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> covered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		return CoverFunction.covered(prob, concept, covExs);
	}



	
	/**
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	static OWLClassExpression refine(LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement;
		do {
			if (currConcept.equals(prob.top))
				refinement = RandomGenerator.getRandomConcept(prob);
			else {
				HashSet<OWLClassExpression> set = new HashSet<OWLClassExpression>();
				set.add(currConcept);
				set.add(RandomGenerator.getSubsumedRandomConcept(prob,currConcept)); // was getRandomConcept
				refinement = prob.dataFactory.getOWLObjectIntersectionOf(set);
			}
			
	//	} while (prob.reasoner.getInstances(refinement,false).entities().count()==0); // found non deprecated method
		} while (!prob.reasoner.isSatisfiable(refinement));		

		return refinement;
	}

	
	
	
		
	

	
} // class
