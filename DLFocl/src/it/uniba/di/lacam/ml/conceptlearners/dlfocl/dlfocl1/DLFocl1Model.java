package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl1;

import java.util.ArrayList;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.DLFoil;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.DLFoil2;

/**
 * DL-Focl1: an iterative hill-climbing for concept learning 
 * @author Giuseppe
 *
 */

public class DLFocl1Model extends CEClassificationModel {

	public DLFocl1Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	}
	
	
	
	/**
	 * Iterative hill-climbing based on DL-Foil
	 * 
	 */
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		//int score =-1;
		
		int omissions =0;
		int nOfClimbs=problem.nOfclimbings;
		System.out.println("Starting repeated hill-climbing search. Maximum number of trials: "+nOfClimbs);
		do {
		if (problem.LENGTH!=-1)
			model = DLFoil.induceConcept(problem, posExs, negExs, undExs); 
		else
			model = DLFoil2.induceConcept(problem, posExs, negExs, undExs);
		
		// reduce ommission rate
		for (int i=0; i<posExs.size(); i++){
			int classify=classify(problem.allIndividuals[posExs.get(i)]);
			omissions= classify==0?(omissions+1):omissions; // omission
		}
				
		for (int i=0; i<negExs.size(); i++){
			int classify=classify(problem.allIndividuals[negExs.get(i)]);
			omissions= classify==0?(omissions+1):omissions; // omission
		}
		nOfClimbs--;
		}
		while( omissions > 5 &&nOfClimbs>0); 
	};


}
