package it.uniba.di.lacam.ml.conceptlearners.refops;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;
import org.semanticweb.owlapi.reasoner.NodeSet;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;

/**
 * The same of RefinementOperator but with more construct and iterative-deepening strategy
 * @author Giuseppe
 *
 */
public class RefinementOperator2 {

	private static int expressiveness=1;

	public static OWLClassExpression target;
	/**
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	static OWLClassExpression rhoSingle(Random  generator, LProblem prob, OWLClassExpression currConcept) {


		OWLClassExpression refinement=null,	filler = null, refinedFiller=null;
		OWLObjectProperty property;

		if (generator.nextFloat() <0.75)
			// refine concept
			switch (currConcept.getClassExpressionType()) {

			case OWL_CLASS: 
				//System.out.println("+");
				NodeSet<OWLClass> subClasses = prob.reasoner.getSubClasses(currConcept,false);
				//System.out.println("SubClasses "+subClasses);
				OWLClass[] subCs = (OWLClass[]) subClasses.entities().filter((f)->f.toString().compareTo(currConcept.toString())!=0).toArray(size -> new OWLClass[size]); 
				//only proper refinements
//			    int max =0;
//				for (OWLClass subC:subCs) {
//					
//					 int nInst= prob.reasoner.getInstances(subC).getFlattened().size();
//					 //System.out.println("**"+subC +"nInst "+nInst);
//					 
//					 if (nInst>=max){
//						 refinement=subC;
//						 max =nInst;
//					 } 
//					 
//				 }
//				 
				
//				if (subCs.length!=0)
					refinement = (OWLClassExpression) subCs[generator.nextInt(subCs.length)];
				//System.out.println("*"+refinement+"*");
				break;
			case OBJECT_COMPLEMENT_OF: 
				OWLClassExpression nestedConcept =  ((OWLObjectComplementOf) currConcept).getOperand();
				NodeSet<OWLClass> superClasses = prob.reasoner.getSuperClasses(nestedConcept,false);
				//System.out.println("Superclasses "+superClasses);
				OWLClass[] supCs = (OWLClass[]) superClasses.entities().filter((f)->f.compareTo(nestedConcept)!=0 && f.toString().compareTo(prob.testConcepts[0].asOWLClass().toString())!=0).toArray(size -> new OWLClass[size]);

		
				refinement = prob.dataFactory.getOWLObjectComplementOf(supCs[generator.nextInt(supCs.length)]); 
				//System.out.println("*******"+refinement);
				break;
			case OBJECT_ALL_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectAllValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectAllValuesFrom(property, refinedFiller);
				break;
			case OBJECT_SOME_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectSomeValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectSomeValuesFrom(property, refinedFiller);
				break;

//							case OBJECT_HAS_VALUE: 
//								
//								prob.dataFactory.getOWLObjectHasValue(property, prob.allIndividuals[generator.nextInt(prob.allIndividuals.length)]);
							//case DATA_SOME_VALUES_FROM:
								
				
								
								
								
								//break;

				//			case DATA_ALL_VALUES_FROM:
				//				break;
				//			case DATA_EXACT_CARDINALITY:
				//				break;
				//			case DATA_HAS_VALUE:
				//				break;
				//			case DATA_MAX_CARDINALITY:
				//				break;
				//			case DATA_MIN_CARDINALITY:
				//				break;
				//			case DATA_SOME_VALUES_FROM:
				//				break;
				//			case OBJECT_EXACT_CARDINALITY:
				//				break;
				//			case OBJECT_HAS_SELF:
				//				break;
				//			case OBJECT_HAS_VALUE:
				//				break;
				//			case OBJECT_INTERSECTION_OF:
				//				break;
				//			case OBJECT_MAX_CARDINALITY:
				//				break;
				//			case OBJECT_MIN_CARDINALITY:
				//				break;
				//			case OBJECT_ONE_OF:
				//				break;
				//			case OBJECT_UNION_OF:
				//				break;
			default:
				break;
			}
		else { // refine by adding conjunct
			OWLClassExpression newConcept = null;
			//int i = 4;
			int nextInt = RandomGenerator.generator.nextInt(expressiveness);
			
			//System.out.println("Type of refs: "+(nextInt));
			switch (nextInt) {

			case 0:
				
				newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
				
				break;

			case 1:
				newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
				newConcept = prob.dataFactory.getOWLObjectComplementOf(newConcept);
				//System.out.println("++++++++++++++++++++++++++++++++"+newConcept);	
			break;
			case 2:
				//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
				filler=prob.top;
				property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
				newConcept = prob.dataFactory.getOWLObjectAllValuesFrom(property, filler);					
			break;
			case 3:
				//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
				filler=prob.top;
				property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
				newConcept = prob.dataFactory.getOWLObjectSomeValuesFrom(property, filler);	
				break;
				//				remove concept for other constructs
//									case 4:
//									//System.out.println("*");
//									List<OWLLiteral> ls=  new  ArrayList<>();
//				
//									while (ls.size()==0) {
//									OWLDataProperty prop2= prob.allDataProperties[RandomGenerator.generator.nextInt(prob.allDataProperties.length)];
//													    //System.out.println(ls);
//									ls =new ArrayList<OWLLiteral>(prob.dpvalues.column(prop2).values());
//									if (ls.size()>0) {
//										OWLDataRange range= prob.dataFactory.getOWLDataOneOf(ls.get(RandomGenerator.generator.nextInt(ls.size())));
//									    newConcept = prob.dataFactory.getOWLDataSomeValuesFrom(prop2,range);
//									}
//									}
//									//System.out.println("****************"+datatypesInSignature);
//									
//									break;
//									
//								case 5:
//									property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
//									newConcept= prob.dataFactory.getOWLObjectHasValue(property, prob.allIndividuals[generator.nextInt(prob.allIndividuals.length)]);

			default:
				break;
			} // switch

			if (currConcept.equals(prob.top))
				refinement = newConcept;
			else {
				HashSet<OWLClassExpression> set = new HashSet<OWLClassExpression>();
				set.add(currConcept);
				set.add(newConcept); // was getRandomConcept
				refinement = prob.dataFactory.getOWLObjectIntersectionOf(set);
			}
			
			
		} // else add

		return refinement;

	}

	// refine conjunct of CEs 
	/**
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	static OWLClassExpression rhoCompound(Random generator, LProblem prob, OWLClassExpression currConcept) {

		
		OWLClassExpression refinement=null;


		HashSet<OWLClassExpression> conjSet = (HashSet<OWLClassExpression>) currConcept.asConjunctSet();
		
		OWLClassExpression[] conjArray = conjSet.toArray(new OWLClassExpression[conjSet.size()]);	

		// select one of the CEs
		int selectedConjIndex = generator.nextInt(conjArray.length);
		OWLClassExpression conjunct2refine = conjArray[selectedConjIndex];
		// refine selected CE
		OWLClassExpression refinedConjunct = rho(generator,prob, conjunct2refine);
		// replace selected CE with the refinement 
		conjSet.remove(conjunct2refine);
		conjSet.add(refinedConjunct);

		refinement = prob.dataFactory.getOWLObjectIntersectionOf(conjSet);

		
		return refinement.getNNF();
	}



	/**
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	public static OWLClassExpression rho(Random generator, LProblem prob, OWLClassExpression currConcept) {

		OWLClassExpression refinement;

		do {

			if (currConcept.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF)  
				// refine one of the conjuncts in the CE 
				refinement = rhoCompound(generator, prob, currConcept);
			else
				
				// replace with a refinement or add a new conjunct
				refinement = rhoSingle(generator,prob, currConcept);

			//		} while (prob.reasoner.getInstances(refinement,false).entities().count()==0); // instead of deprecated method
		} while (!prob.reasoner.isSatisfiable(refinement) || refinement.containsConjunct(prob.testConcepts[0]));		

		return refinement;
	}

	public static void increaseExpressiveness() {
		// TODO Auto-generated method stub
		expressiveness= expressiveness<4? expressiveness+1: expressiveness;
		//expressiveness =4;
		
	}



}
