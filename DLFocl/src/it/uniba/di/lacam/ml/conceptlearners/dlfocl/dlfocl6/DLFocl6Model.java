package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl6;

import java.util.ArrayList;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;

/**
 * Wrapper for the new version of tabu search with timestamps
 * @author Giuseppe
 *
 */
public class DLFocl6Model extends CEClassificationModel {
	

	public DLFocl6Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		
	
		System.out.println("Starting local search with tabu list");
		
		model = DLFocl6.induceConcept(problem, posExs, negExs, undExs); 
		
				 
	};


}
