package it.uniba.di.lacam.ml.conceptlearners.dlfoil;

import org.semanticweb.owlapi.model.OWLClassExpression;

import it.uniba.di.lacam.ml.conceptlearners.Evaluation;
import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator2;

public class DLFoilTest {
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("--->");
	
		LProblem problem = new LProblem(args[0]);

		OWLClassExpression exp=problem.top;//problem.testConcepts[0];
		RefinementOperator2.target=exp;
		
		if (problem.design.compareToIgnoreCase("cv")==0)
			Evaluation.crossValidation(problem); // run an experiment session
		else
			Evaluation.bootstrap(problem);
		
		System.out.print("\n\nEND: "+problem.urlOwlFile);
		System.out.printf("\t Seed: %d", problem.seed);


	} // main
	
	
	
} // class DLFoilTest