package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl2;

import java.util.ArrayList;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;

/**
 * A wrapper class for DLFocl2 
 * @author Giuseppe Rizzo
 *
 */
public class DLFocl2Model extends CEClassificationModel {

	public DLFocl2Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	
	}
	
	
	/**
	 * invoke the procedure induceConcept
	 */
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		
	
		System.out.println("DL-Focl2: Starting hill-climbing with lookahead");
		model = DLFocl2.induceConcept(problem, posExs, negExs, undExs); 
		
				 
	};


}
