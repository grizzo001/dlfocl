package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl3;

import java.util.ArrayList;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.CEClassificationModel;

/**
 * Wrapper for DL-Focl3 (to preserve the compatibility with DL-Foil interface)
 * @author Giuseppe
 *
 */
public class DLFocl3Model extends CEClassificationModel {
	

	public DLFocl3Model(LProblem aProblem) {
		super(aProblem);
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		
	
		System.out.println("Starting local search with tabu list");
		
		model = DLFocl3.induceConcept(problem, posExs, negExs, undExs); 
		
				 
	};


}
