package it.uniba.di.lacam.ml.conceptlearners.tdt;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.scores.Heuristics;

import java.util.HashSet;
import java.util.ArrayList;



/**
 *
 * Induces a terminological decision tree
 * @author NF
 *
 */
public class TDTLearner {
	
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer(); 

	/**
	 * TDT induction algorithm implementation
	 * 
	 * @param prob Learning problem
	 * @param father father concept
	 * @param posExs positive examples
	 * @param negExs negative examples
	 * @param undExs unknown m. examples
	 * @param nCandRefs 
	 * @param prPos , priors
	 * @param prNeg, priors
	 * @return
	 */
	static TDT induceDLTree	(LProblem prob, OWLClassExpression father, 
				ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs, 
				int nCandRefs, double prPos, double prNeg) {		
		
		final double THRESHOLD = 0.05;
		
		System.out.printf("\n * Learning problem\t p:%d\t n:%d\t u:%d\t prPos:%4f\t prNeg:%4f\n", 
				posExs.size(), negExs.size(), undExs.size(), prPos, prNeg);
		
		TDT tree = new TDT(); // new (sub)tree		

		if (posExs.size() == 0 && negExs.size() == 0) // no exs
			if (prPos >= prNeg) { // prior majority of positives
				tree.setRoot(prob.top); // set positive leaf
				System.out.println("-----\nPOS leaf (prior)");
				return tree;
			}
			else { // prior majority of negatives
				tree.setRoot(prob.dataFactory.getOWLNothing()); // set negative leaf
				System.out.println("-----\nNEG leaf (prior)");
				return tree;
			}
		
//		double numPos = posExs.size() + undExs.size()*prPos;
//		double numNeg = negExs.size() + undExs.size()*prNeg;
		double numPos = posExs.size();
		double numNeg = negExs.size();
		double perPos = numPos/(numPos+numNeg);
		double perNeg = numNeg/(numPos+numNeg);

		if (perNeg==0 && perPos > THRESHOLD) { // no negative
			tree.setRoot(prob.top); // set positive leaf
			System.out.println("-----\nPOS leaf (thr)\n");
			return tree;
			}
		else 
		if (perPos==0 && perNeg > THRESHOLD) { // no positive			
			tree.setRoot(prob.dataFactory.getOWLNothing()); // set negative leaf
			System.out.println("-----\nNEG leaf (thr)\n");
			return tree;
		}		
		// else (a non-leaf node) ...
		
		
		
		OWLClassExpression[] cConcepts = generateRefs(prob, father, nCandRefs, posExs, negExs);
		
		// select node concept
		OWLClassExpression bestConcept = selectBestConcept(prob, cConcepts, posExs, negExs, undExs, prPos, prNeg);
		
		ArrayList<Integer> posExsL = new ArrayList<Integer>();
		ArrayList<Integer> negExsL = new ArrayList<Integer>();
		ArrayList<Integer> undExsL = new ArrayList<Integer>();
		ArrayList<Integer> posExsR = new ArrayList<Integer>();
		ArrayList<Integer> negExsR = new ArrayList<Integer>();
		ArrayList<Integer> undExsR = new ArrayList<Integer>();
		
		split(prob, bestConcept, 
				posExs, negExs, undExs, 
				posExsL, negExsL, undExsL, 
				posExsR, negExsR, undExsR);
		// select node concept
		tree.setRoot(bestConcept.getNNF());		
		// build subtrees
		
		
		tree.setLSubTree(induceDLTree(prob, bestConcept, posExsL, negExsL, undExsL, nCandRefs, prPos, prNeg));
		tree.setRSubTree(induceDLTree(prob, bestConcept.getComplementNNF(), posExsR, negExsR, undExsR, nCandRefs, prPos, prNeg));
				
		return tree;
	}



	/**
	 * routine selecting the best in a list (array) of refinements 
	 * @param prob
	 * @param concepts
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @param prPos
	 * @param prNeg
	 * @return
	 */
	private static OWLClassExpression selectBestConcept(LProblem prob, OWLClassExpression[] concepts,
			ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs, 
			double prPos, double prNeg) {

		int[] counts;

		
		int bestConceptIndex = 0;
		
		counts = getSplitCounts(prob, concepts[0], posExs, negExs, undExs);
		System.out.printf("%4s\tp:%4d\t n:%4d\t u:%4d\t --- p:%4d\t n:%4d\t u:%4d\t ", 
				"#"+0, counts[Heuristics.PL], counts[Heuristics.NL], counts[Heuristics.UL], counts[Heuristics.PR], counts[Heuristics.NR], counts[Heuristics.UR]);
		
//		System.out.println(concepts[0]);		
		
		double bestGain = Heuristics.twoing(counts);
		System.out.printf("%10f\n",bestGain);
			
		for (int c=1; c<concepts.length; c++) {
			
			counts = getSplitCounts(prob, concepts[c], posExs, negExs, undExs);
			System.out.printf("%4s\tp:%4d\t n:%4d\t u:%4d\t --- p:%4d\t n:%4d\t u:%4d\t ", 
					"#"+c, counts[Heuristics.PL], counts[Heuristics.NL], counts[Heuristics.UL], counts[Heuristics.PR], counts[Heuristics.NR], counts[Heuristics.UR]);
			
			double thisGain = Heuristics.twoing(counts);
			System.out.printf("%10f\n",thisGain);
//			System.out.println(concepts[c]);
			
			if(thisGain > bestGain) {
				bestConceptIndex = c;
				bestGain = thisGain;
			}
		}
		
		System.out.printf("\n -------- best gain: %f \t split #%d\n %s\n\n", bestGain, bestConceptIndex, TDT.renderer.render(concepts[bestConceptIndex]));
		return concepts[bestConceptIndex];
	}



	
	/**
	 * @param counts
	 * @return
	 */
	private static double gain(int[] counts) {
		return Heuristics.twoing(counts);
	}
	
//	private static double gini(double numPos, double numNeg, double numUnd) {
//		
//		double sum = numPos+numNeg;
//		
//		if (sum>0) {
//			double p1 = numPos/sum;
//			double p2 = numNeg/sum;
//			double p3 = numUnd/sum;
//
//			return (1.0-p1*p1-p2*p2);
//		}
//		else 
//			return (Double.MAX_VALUE);
//
//	}
	
	
	/**
	 * @param prob
	 * @param concept
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @return
	 */
	private static int[] getSplitCounts(LProblem prob, OWLClassExpression concept, 
			ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		
		int[] counts = new int[6];

		ArrayList<Integer> posExsL = new ArrayList<Integer>();
		ArrayList<Integer> negExsL = new ArrayList<Integer>();
		ArrayList<Integer> undExsL = new ArrayList<Integer>();
		ArrayList<Integer> posExsR = new ArrayList<Integer>();
		ArrayList<Integer> negExsR = new ArrayList<Integer>();
		ArrayList<Integer> undExsR = new ArrayList<Integer>();
		
		splitGroup(prob, concept, posExs, posExsL, posExsR);
		splitGroup(prob, concept, negExs, negExsL, negExsR);	
		splitGroup(prob, concept, undExs, undExsL, undExsR);	
		
		counts[Heuristics.PL] = posExsL.size(); 
		counts[Heuristics.NL] = negExsL.size(); 
		counts[Heuristics.UL] = undExsL.size(); 
		counts[Heuristics.PR] = posExsR.size(); 
		counts[Heuristics.NR] = negExsR.size();
		counts[Heuristics.UR] = undExsR.size();
		
		return counts;
		
	}
	
	/**
	 * @param prob
	 * @param concept
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @param posExsL
	 * @param negExsL
	 * @param undExsL
	 * @param posExsR
	 * @param negExsR
	 * @param undExsR
	 */
	private static void split(LProblem prob, OWLClassExpression concept,
			ArrayList<Integer> posExs,  ArrayList<Integer> negExs,  ArrayList<Integer> undExs,
			ArrayList<Integer> posExsL, ArrayList<Integer> negExsL,	ArrayList<Integer> undExsL, 
			ArrayList<Integer> posExsR,	ArrayList<Integer> negExsR, ArrayList<Integer> undExsR) {
				
		splitGroup(prob, concept, posExs, posExsL, posExsR);
		splitGroup(prob, concept, negExs, negExsL, negExsR);
		splitGroup(prob, concept, undExs, undExsL, undExsR);	
		
	}


	/**
	 * @param prob
	 * @param concept
	 * @param nodeExamples
	 * @param leftExs
	 * @param rightExs
	 */
	private static void splitGroup(LProblem prob, OWLClassExpression concept, ArrayList<Integer> nodeExamples,
			ArrayList<Integer> leftExs, ArrayList<Integer> rightExs) {

		OWLClassExpression negConcept = prob.dataFactory.getOWLObjectComplementOf(concept);
		
		for (int e=0; e<nodeExamples.size(); e++) {
			int exIndex = nodeExamples.get(e);
			if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(concept, prob.allIndividuals[exIndex])))
				leftExs.add(exIndex);
			else if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(negConcept, prob.allIndividuals[exIndex])))
				rightExs.add(exIndex);
			else { 
				leftExs.add(exIndex); 
				rightExs.add(exIndex); 
			}		
		}	
	}


	/**
	 * @param prob
	 * @param concept
	 * @param dim
	 * @param posExs
	 * @param negExs
	 * @return
	 */
	private static OWLClassExpression[] generateRefs(LProblem prob, OWLClassExpression concept, int dim, ArrayList<Integer> posExs, ArrayList<Integer> negExs) {
		
		System.out.printf("\nGenerating node concepts ");
		OWLClassExpression[] rConcepts = new OWLClassExpression[dim];
		OWLClassExpression newConcept, refinement;
		boolean emptyIntersection;
		for (int c=0; c<dim; c++) {
			do {
				emptyIntersection = false; // true
				refinement = RandomGenerator.getRandomConcept(prob);
            	HashSet<OWLClassExpression> newConcepts = new HashSet<OWLClassExpression>();	            	
            	newConcepts.add(concept);
            	newConcepts.add(refinement);
            	newConcept = prob.dataFactory.getOWLObjectIntersectionOf(newConcepts);
            	
//				Iterator<OWLIndividual> instIterator = reasoner.getIndividuals(newConcept, false).iterator();
//				emptyIntersection = reasoner.getIndividuals(newConcept, false).size()<1;
				emptyIntersection = !prob.reasoner.isSatisfiable(newConcept);

//				while (emptyIntersection && instIterator.hasNext()) {
//					OWLIndividual nextInd = (OWLIndividual) instIterator.next();
//					int index = -1;
//					for (int i=0; index<0 && i<allIndividuals.length; ++i)
//						if (nextInd.equals(allIndividuals[i])) index = i;
//					if (posExs.contains(index))
//						emptyIntersection = false;
//					else if (negExs.contains(index))
//						emptyIntersection = false;
//				}					
			} while (emptyIntersection);
			rConcepts[c] = newConcept; // normalized ?
			System.out.printf("%d ", c);
		}
		System.out.println();
		
		return rConcepts;
	}
	
	
	

	
	
	

	
} // class DLTreeInducer