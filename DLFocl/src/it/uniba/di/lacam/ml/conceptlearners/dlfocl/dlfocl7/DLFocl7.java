/**
 * 
 */
package it.uniba.di.lacam.ml.conceptlearners.dlfocl.dlfocl7;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils.AbstractTabuList;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils.ProbabilisticTabuList;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils.TabuList;
import it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils.TimeTabuList;
import it.uniba.di.lacam.ml.conceptlearners.dlfoil.DLFoil;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator2;
import it.uniba.di.lacam.ml.conceptlearners.scores.Heuristics;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;



/**
 * 
 * A new version of DL-Focl 3 with new ref.op using datatype properties and a long term memory
 * @author Giuseppe Rizzo
 *
 */
public class DLFocl7 extends DLFoil {

	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer(); 
	static AbstractTabuList tabu;
	static int maxTrials; 




	/**
	 * @param prob
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @return
	 */
	public static OWLClassExpression induceConcept(LProblem prob, ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {		
		tabu= new ProbabilisticTabuList(prob);
		System.out.println("***** DL-Focl VI--- Time-based tabu list *****");

		System.out.printf("\n Problem\t p:%d\t n:%d\t u:%d\t\n", 
				posExs.size(), negExs.size(), undExs.size());

		// set of disjuncts that make up the final concept
		HashSet<OWLClassExpression> disjuncts = new HashSet<OWLClassExpression>(); 

		ArrayList<Integer> pos2Cover = (ArrayList<Integer>)posExs.clone();
		OWLClassExpression newConcept = prob.top;
		OWLClassExpression finalConcept = prob.dataFactory.getOWLNothing(); // empty set --> bottom 

		ArrayList<Integer> covPos = null, covNeg = null, covUnd = null;		
		while (pos2Cover.size()>prob.MIN_POS2COV) { //&& (currentTime2-currentTime1)<200000) {		

			covPos = (ArrayList<Integer>)pos2Cover.clone();
			covNeg = (ArrayList<Integer>)negExs.clone();
			covUnd = (ArrayList<Integer>)undExs.clone();
			maxTrials = 0; // reinitializes the number of trials
			System.out.println("Subproblem ------------------------------------------");
			OWLClassExpression oldConcept= prob.top;
			while (covPos.size() > prob.MIN_COV_POS && covNeg.size()+covUnd.size() > prob.MIN_COV_NEG && maxTrials <prob.TRIALS) {

				newConcept = getBestSpecialization(prob, newConcept, covPos, covNeg, covUnd, tabu);
				if (newConcept!=null) {
					covPos = covered(prob, newConcept, covPos);
					covNeg = covered(prob, newConcept, covNeg);
					covUnd = covered(prob, newConcept, covUnd);

					System.out.printf("\nSpecialization, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
					System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());

				} //System.out.println("No concept found");

			}

			if (newConcept != null) {
				System.out.printf("\nDisjunct, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
				System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());
				tabu.add(newConcept);
				disjuncts.add(newConcept);
				pos2Cover = removeCovered(prob, newConcept, pos2Cover);
			}

			newConcept = prob.top;
			//tabu.remove();  

		}	

		if (disjuncts.size() == 1) 
			finalConcept = disjuncts.iterator().next(); // single disj.
		else 
			finalConcept = prob.dataFactory.getOWLObjectUnionOf(disjuncts); // multiple disj. 

		System.out.printf("\n FINAL: %s\n\n", renderer.render(finalConcept));
		return finalConcept;

	}


	/**
	 * @param prob
	 * @param currConcept
	 * @param covPos
	 * @param covNeg
	 * @param covUnd
	 * @param tabuList 
	 * @return
	 */
	public static OWLClassExpression getBestSpecialization(LProblem prob, OWLClassExpression currConcept, 
			ArrayList<Integer> covPos, ArrayList<Integer> covNeg, ArrayList<Integer> covUnd, AbstractTabuList tabuList) {

		//int maxTrials=0;

		OWLClassExpression bestConcept = null;
		double bestGain = - Double.MAX_VALUE;	


		
		while (bestGain <= 0 && maxTrials<4) {
			System.out.printf("\nSpecializing %s\n %d\t %d\t %d\n", 
					renderer.render(currConcept), covPos.size(), covNeg.size(), covUnd.size());

			

			for (int c=0; c<(prob.nCandSubConcepts); ++c) {
				ArrayList<Integer> 	newCovPos=null, newCovNeg=null, newCovUnd=null; 


				do {
					OWLClassExpression refConcept = RefinementOperator.rho(RandomGenerator.generator,prob, currConcept);


					double thisGain = 0;
					// check the tabu list


					newCovPos = covered(prob, refConcept, covPos);
					if ((tabuList.isNotAdmissible(refConcept)))
						continue;
					if (newCovPos.size()>0) {
						newCovNeg = covered(prob, refConcept, covNeg);
						newCovUnd = covered(prob, refConcept, covUnd);

						thisGain = Heuristics.wig(covPos.size(), covNeg.size(), covUnd.size(), 
								newCovPos.size(), newCovNeg.size(), newCovUnd.size());

						System.out.printf("%4d. %s\n\t%4d\t %4d\t %4d\t\t %+7e", 
								c, renderer.render(refConcept), newCovPos.size(), newCovNeg.size(), newCovUnd.size(), thisGain);

						// memorizes the new refs.
						if (thisGain > bestGain && thisGain > 0) {
							bestConcept = refConcept;
							bestGain = thisGain;
							System.out.println(" <--- current BEST");
						}
						else {

							tabu.add(refConcept); 
							System.out.println();
						}
					}

				} while (newCovPos.size()==0);

			}//
			if (bestGain <= 0) {
				maxTrials++;
				System.out.print("\nNo good specialization found. Trying again... \n");
				RefinementOperator2.increaseExpressiveness();
				tabu.remove(); 
			}
		} // while


		String bestString= bestConcept!=null?renderer.render(bestConcept):"";

		System.out.printf("************ best: %e\t %s\n", bestGain, bestString);
		tabu.add(bestConcept);
		 // remove the eldest element

		return bestConcept;
	}

	



} // class
