package it.uniba.di.lacam.ml.conceptlearners.dlfocl.utils;

import org.semanticweb.owlapi.model.OWLClassExpression;

public abstract class AbstractTabuList {

	
	
	public abstract boolean add(OWLClassExpression c);
	
	
	public abstract boolean isNotAdmissible(OWLClassExpression c); 

	public abstract boolean remove();
	
	public abstract long size();

}
