/**
 * 
 */
package it.uniba.di.lacam.ml.conceptlearners.dlfoil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;

import it.uniba.di.lacam.ml.conceptlearners.LProblem;
import it.uniba.di.lacam.ml.conceptlearners.RandomGenerator;
import it.uniba.di.lacam.ml.conceptlearners.coverfunctions.CoverFunction;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator;
import it.uniba.di.lacam.ml.conceptlearners.refops.RefinementOperator2;
import it.uniba.di.lacam.ml.conceptlearners.scores.Heuristics;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;



/**
 * A modified version of DL-Foil with a new refinement operator exploiting 
 * datatype property values and iterative-deepening strategy for generating specializations
 * @author Giuseppe Rizzo
 *
 */
public class DLFoil2 {

	private static final int TIMEOUT = 200000;
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer(); 
	public static boolean fail;
	static long now= System.currentTimeMillis();
	static long start = System.currentTimeMillis();

	
	/**
	 * @param prob
	 * @param posExs
	 * @param negExs
	 * @param undExs
	 * @return
	 */
	public static OWLClassExpression induceConcept(LProblem prob, ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {		
		
		System.out.printf("\n Problem\t p:%d\t n:%d\t u:%d\t\n", 
				posExs.size(), negExs.size(), undExs.size());
		
		// set of disjuncts that make up the final concept
		HashSet<OWLClassExpression> disjuncts = new HashSet<OWLClassExpression>(); 
		
		ArrayList<Integer> pos2Cover = (ArrayList<Integer>)posExs.clone();
		OWLClassExpression newConcept = prob.top;
		OWLClassExpression finalConcept = prob.dataFactory.getOWLNothing(); // empty set --> bottom 
		
		fail= false;
		ArrayList<Integer> covPos = null, covNeg = null, covUnd = null;	
		int i=0;
		boolean maxL=false;
		
		//double d = (double)pos2Cover.size()/posExs.size();
		while (pos2Cover.size()>prob.MIN_POS2COV) { //&&  {		
		
			
			covPos = (ArrayList<Integer>)pos2Cover.clone();
			covNeg = (ArrayList<Integer>)negExs.clone();
			covUnd = (ArrayList<Integer>)undExs.clone();
			
			
			System.out.println("Subproblem ------------------------------------------");
			while ((covPos.size() > prob.MIN_COV_POS && covNeg.size()>prob.MIN_COV_NEG)) {
				
				newConcept = getBestSpecialization(prob, newConcept, covPos, covNeg, covUnd);
				covPos = CoverFunction.covered(prob, newConcept, covPos);
				covNeg = CoverFunction.covered(prob, newConcept, covNeg);
				//covUnd = CoverFunction.covered(prob, newConcept, covUnd);
				System.out.printf("\nSpecialization, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
				System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());
				
			}

			System.out.printf("\nDisjunct, covering P:%s N:%s U:%s\n", covPos.size(), covNeg.size(), covUnd.size());
			System.out.printf("%s\n\n", renderer.render(newConcept), covPos.size(), covNeg.size(), covUnd.size());

			disjuncts.add(newConcept);
			pos2Cover = CoverFunction.removeCovered(prob, newConcept, pos2Cover);
			//d=(double)pos2Cover.size()/posExs.size();
			newConcept = prob.top;
			i++;
		
			
		}	
		
		if (disjuncts.size() == 1) 
			finalConcept = disjuncts.iterator().next(); // single disj.
		else {
			disjuncts.remove(prob.top); // remove useless disjuncts
			finalConcept = prob.dataFactory.getOWLObjectUnionOf(disjuncts); // multiple disj. 
		}
		System.out.printf("\n FINAL: %s\n\n", renderer.render(finalConcept));
		return finalConcept;
		
	}
	
	
	/**
	 * @param prob
	 * @param currConcept
	 * @param covPos
	 * @param covNeg
	 * @param covUnd
	 * @return
	 */
	protected static OWLClassExpression getBestSpecialization(LProblem prob, OWLClassExpression currConcept, 
			ArrayList<Integer> covPos, ArrayList<Integer> covNeg, ArrayList<Integer> covUnd) {
	
		
	OWLClassExpression bestConcept = null;
		
	double initialGain= 0;
	double bestGain = - Double.MAX_VALUE;
	int nCovNeg=covNeg.size();
    int nnCovNeg=0;
	int i=0;
	
	while ( bestGain<= initialGain) {
		System.out.printf("\nSpecializing %s\n %d\t %d\t %d\n", 
							renderer.render(currConcept), covPos.size(), covNeg.size(), covUnd.size());	
		now = System.currentTimeMillis();
		
		for (int c=0; c<(prob.nCandSubConcepts); ++c) {
			ArrayList<Integer> 	newCovPos=null, newCovNeg=null, newCovUnd=null; 
			//System.out.printf( "Generating  %d-th concept\n", c);
			OWLClassExpression refConcept;
			int t=0;
			
			do {
				refConcept = RefinementOperator2.rho(RandomGenerator.generator,prob, currConcept);
				
				// add a lookhead 
				//refConcept = rho(prob, refConcept);
				
				
				double thisGain = 0;
			
				newCovPos = CoverFunction.covered(prob, refConcept, covPos);
				//System.out.println(renderer.render(refConcept)+"      "+newCovPos);
				if (newCovPos.size()>0) {
					newCovNeg = CoverFunction.covered(prob, refConcept, covNeg);
					newCovUnd = CoverFunction.covered(prob, refConcept, covUnd);
		
					thisGain = Heuristics.wig(covPos.size(), covNeg.size(), covUnd.size(), 
								   newCovPos.size(), newCovNeg.size(), newCovUnd.size());
					
					System.out.printf("%4d. %s\n\t%4d\t %4d\t %4d\t\t %+7e", 
							c, renderer.render(refConcept), newCovPos.size(), newCovNeg.size(), newCovUnd.size(), thisGain);
						
					if (thisGain > bestGain && thisGain >initialGain) {
						bestConcept = refConcept;
						bestGain = thisGain;
						System.out.println(" <--- current BEST");
					}
					else
						System.out.println();
				}
				
				
				t++;
			} while (newCovPos.size()==0);
		} //
		
		System.out.println(bestGain);
		if (bestGain <=initialGain || (bestConcept!=null&& currConcept.containsConjunct(bestConcept))) {
			System.out.print("\nNo good specialization found. Trying again... \n");
			//initialGain=bestGain;
			RefinementOperator2.increaseExpressiveness();
		}
	} // while
	
	

	System.out.printf("************ best: %e\t %s\n", bestGain, renderer.render(bestConcept));
	
	return bestConcept;
	}
	
	
	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> removeCovered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		return CoverFunction.removeCovered(prob, concept, covExs);
	}
	
	
	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> covered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		return CoverFunction.covered(prob, concept, covExs);
	}



	
	/**
	 * @param prob
	 * @param currConcept
	 * @return
	 */
	static OWLClassExpression refine(LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement;
		do {
			if (currConcept.equals(prob.top))
				refinement = RandomGenerator.getRandomConcept(prob);
			else {
				HashSet<OWLClassExpression> set = new HashSet<OWLClassExpression>();
				set.add(currConcept);
				set.add(RandomGenerator.getSubsumedRandomConcept(prob,currConcept)); // was getRandomConcept
				refinement = prob.dataFactory.getOWLObjectIntersectionOf(set);
			}
			
	//	} while (prob.reasoner.getInstances(refinement,false).entities().count()==0); // found non deprecated method
		} while (!prob.reasoner.isSatisfiable(refinement));		

		return refinement;
	}

	
	
	
		
	

	
} // class
