# README #

A concept learner OWL2  in Java using JFact+OWLAPI 5 to avoid sub-optimal solutions. The learner integrates various strategies:
* DL-Focl I:  a repeated hill-climbing strategy to mitigate the number of   classifications w.r.t. the learned concept
* DL-Focl II: an hill-climbing strategy with a lookahead to improve the selection of best partial definitions
* DL- Focl III:  an hill-climbing integrating a tabu search with a short-term memory

### What is this repository for? ###

* Concept Learner in Description Logics (OWL2)
* 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
 - Clone the repository via Git
 - Import the project as a Maven project
* Dependencies
 1. main: JFact 5.0.0, OWLAPI 5
 2. other: apache-commons - lang3, math3, configuration2, beanutils
 All the required libraries can be  imported through Maven 

### How to run?###
The  concept learner can be configured through an xml file,  where the user can specify the configuration of the algorithms and the design of the experiments (cross-validation/bootstrap)
An example of configuration file is reported below:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<config>

<kb>
<source>file:////C:/Users/Giuseppe/ontos/NTNcombined.owl<source>
<reasoner>jfact</reasoner><!-- currently supported: jfact (default), hermit-->
</kb>

<!-- algorithm section -->
<algo>
	<!-- type of algo -->
	<!-- more algorithms can be run in the experiments (e.g a threewise cmparison among DLFoil, DLFocl1 and TDT learner)-->
	<!-- currently supported: 
	<!--DLFoil, the original dl-foil algorithm 
	    DLFoil2, the same of DL-Foil but with the iterative deepening strategy
	    DLFocl, implementing the repeated hill-climbing strategy !
	    DLFocl2, endowed with a ref.op. with lookahead
	    DLFocl3, implementing the tabu search 
	    DLFocl4, implementing the rep.hill-climbing with match rate maximization,
	    DL-Focl6, implementing the time-base tabu search
	    TDT, implementing the terminological decision tree inducer!---> 
	<types>
	<type>DLFoil</type>
	<type>DLFocl1</type>
	<type>TDT</type>
	</types>
	<!-- num. of candidate refinements per turn -->
	<NC> 50 </NC>
	<!--number of repetitions for DL-Focl1 and 4-->
	<nClimb>3</nClimbs>
	<!--  number of trials for DL-Focl with tabu search -->
	<trials>4</trials>
	<!-- minimum number of positive ex.s to cover -->
	<minPos2Cov>0</minPos2Cov>
	<!-- minimum number of covered positive and negative ex.s -->
	<minCovPos>0</minCovPos>
	<minCovNeg>0</minCovNeg>
	<!--  length constraint for DL-FOCL I length=-1 ==> no constraint -->
	<length>3</length> 
	
</algo>

<!-- seed for random numbers generators -->
<seed> 1 </seed>

<!-- evaluation section (currently supported: .632 bootstrap, cross validation -->
<evaluation>
	<design>cv</design>
	<!-- rand target concepts -->
 	<NRT> 2 </NRT> 
	<!-- num. of repeatitions/ folds  -->
	<NF> 5 </NF>
	
<!-- target class expressions (Manchester syn) section. -->
	<targets>
<!--  Empty if the concepts are randomly generated  -->
	 	<target>
		Agent and (residentPlace some (City or subregion some (not GeographicLocation)))
		</target>
 	 	  <target> 
 		not (Woman) or knows some (not God) 
 		</target>

	</targets>
	<!-- negative example generation policy: allinds, i.e. all individuals are used, siblings, i.e. sibling policy and files. Using files, you must provide (using <exDir></exDir> tag) 
	an absolute path for a directory that must contains two files: 1) pos.txt 2) neg.txt (it is mandatory)
	-->
	<negExs>allinds</negExs> <!--<negExs>allinds</negExs> | <negExs>allinds</negExs> | <negExs>files</negExs>-->
	<exDir>C:/Users/Giuseppe/git/dlfocl/DLFocl/experiments/third session/onto-datasets/hepatitis</exDir> <!-- Only considered  when <negExs>files</negExs>-->

</evaluation>

</config>
```
### Versions:###
 In particular, we adopted the following Git tags to denote the milestone:
 
 	- FGCS_submission_complete_code  (2019-02-11): this tag is used to indicate a milestone, i.e. the source code released before the submission of the first  version of the paper
	- FGCS_submission_repo_reorganization(2019-07-04): this tag is used to label the same source code + further files (logs and ontos) released for reproducibility purposes 
	- FGCS_submission_removed_files(2019-07-13): the tag is used to denote a commit where the repository has been modified removing useless file (owl files different from those described in the paper).
	- FGCS_submission_replaced_yago_file(2019-10-14):  the tag is used to denote a commit where the repository has been fixed replacing a wrong owl file for yago kb(learning problem 2) 
	- FGCS_submission_correctlogs (2019-10-19):  with the correct logs concerning the second experimental session with CELOE
